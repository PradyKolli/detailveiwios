//
//  TBTableViewCell.swift
//  detailviewIOS
//
//  Created by student on 6/21/19.
//  Copyright © 2019 NWMSU. All rights reserved.
//

import UIKit

class TBTableViewCell: UITableViewCell {

    @IBOutlet weak var textTF: UILabel!
    @IBOutlet weak var inputTF: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
