//
//  DetailViewController.swift
//  detailviewIOS
//
//  Created by student on 6/17/19.
//  Copyright © 2019 NWMSU. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    static var selectedIngredients:[String] = []
    var inkypinky:[String] = []
    @IBOutlet weak var tableView: UITableView!
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DetailViewController.selectedIngredients.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectedIngredients", for: indexPath) as! TBTableViewCell
        cell.textTF.text = DetailViewController.selectedIngredients[indexPath.row]
        return cell
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.allowsMultipleSelection = true
    }

    override func viewWillAppear(_ animated: Bool) {
        print(DetailViewController.selectedIngredients.count)
        tableView.reloadData()
    }
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectedIngredients", for: indexPath) as! TBTableViewCell
//        inkypinky.append(cell.inputTF.text!)
//    }
    @IBAction func calculate(_ sender: Any) {
        for i in 0 ..< self.tableView.numberOfRows(inSection: 0){
            let cell: TBTableViewCell = self.tableView.cellForRow(at: IndexPath(row: i, section: 0)) as! TBTableViewCell
            self.inkypinky.append( cell.inputTF.text! )
            
        }
        print(inkypinky)
    }
}

